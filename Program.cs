﻿using System.Numerics;

namespace UniversityLaboratory2._2
{
    internal static class Program
    {
        private static void Main()
        {
            LaboratoryInfo laboratoryInfo = new LaboratoryInfo(
                "Бродский Егор Станиславович",
                "ИР-133Б",
                2,
                "Введення-виведення інформації, з використанням файлів."
            );
            ConsoleMessageGenerator.GenerateLaboratoryTitle(
                laboratoryInfo
            );

            Console.WriteLine();
            TaskChooser.Run(new TaskInfo[]
            {
                new TaskInfo("Номер 1", () =>
                {
                    int a = 34;
                    int i = -6124;
                    double c = (3.2 * Math.Pow(10, 5));
                    bool l = false;
                    string n = "Бродский";
                    
                    Console.WriteLine($"N= {n} L= {l} C= {c} I= {i} A= {a}");
                    return null;
                }),
                new TaskInfo("Номер 2", () =>
                {
                    StreamReader reader = new StreamReader("./LAB2.txt");
                    StreamWriter writer = new StreamWriter("./LAB2.res");
                    writer.WriteLine("Получено");
                    Console.WriteLine("Получено");
                    fileBegin:
                    string? line = reader.ReadLine();
                    if (line == null) goto fileEnd;
                    double s = Convert.ToDouble(line);
                    double result = 4 * s + (1 / (s + 1));
                    writer.WriteLine($"для заданной функции Y({s})= {result,2}");
                    Console.WriteLine($"для заданной функции Y({s})= {result,2}");
                    goto fileBegin;
                    fileEnd:
                    writer.WriteLine($"Составил: {laboratoryInfo.laboratoryOwner}");
                    Console.WriteLine($"Составил: {laboratoryInfo.laboratoryOwner}");
                    reader.Close();
                    writer.Close();
                    return null;
                })
            });
        }
    }
}