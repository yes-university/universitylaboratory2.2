﻿namespace UniversityLaboratory2._2;

public static class Utils
{
    public static int ToInt(string? text)
    {
        return int.Parse(
            string.Join("",
                (new string[] {"0"})
                .Concat(
                    text?.ToCharArray()
                        .Select((c => c.ToString()))
                        .Where((s =>
                                {
                                    try
                                    {
                                        int.Parse(s);
                                        return true;
                                    }
                                    catch (Exception e)
                                    {
                                        return false;
                                    }
                                }
                            ))
                    ?? Array.Empty<string>()
                )
            )
        );
    }
    public static double ToDoubleInt(string? text)
    {
        return Convert.ToDouble(
            string.Join("",
                (new string[] {"0"})
                .Concat(
                    text?.ToCharArray()
                        .Select((c => c.ToString()))
                        .Where((s =>
                                {
                                    try
                                    {
                                        Convert.ToDouble(s);
                                        return true;
                                    }
                                    catch (Exception e)
                                    {
                                        return false;
                                    }
                                }
                            ))
                    ?? Array.Empty<string>()
                )
            )
        );
    }
}