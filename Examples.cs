﻿namespace UniversityLaboratory2._2;

public class Examples
{
    public static void Example1()
    {
        int a = -14;
        float c = -0.00151f;
        double i = 1234.56789;
        bool l = false;
        string name = "Petrov";
        System.Console.WriteLine("name = {0,6}, l = {1,4}", name, l);
        System.Console.WriteLine("a ={0,4}, c = {1,10:f5}, i =  {1,20:e8} ", a, c, i);
        System.Console.WriteLine(" ");
        System.Console.WriteLine("Для виходу натисніть Enter");
        System.Console.ReadLine();
    }

    public static void Example2()
    {
        int a; // = -14; 
        float c; // = -0.00151f; 
        double i; // = 1234.56789; 
        bool l; // = false; 
        string name; //="Petrov"; 

        Console.WriteLine("Input призвіще");
        name = Console.ReadLine();
        Console.WriteLine("Input Ȕ");
        a = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Input c");
        c = Convert.ToSingle(Console.ReadLine());
        Console.WriteLine("Input i");
        i = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Input l");
        l = Convert.ToBoolean(Console.ReadLine());
        System.Console.WriteLine(" Результати форматування \n name = {0,6}, l = {1,4}", name, l);
        System.Console.WriteLine("a ={0, 4}, c =  {1,10:f5}, i = {2,20:e8}", a, c, i);
        System.Console.WriteLine(" ");
        System.Console.WriteLine("Для виходу натисніть Enter");
        System.Console.ReadLine();
    }

    public static void Example3()
    {
        string s;
        double x, y;
        StreamWriter f = new StreamWriter("outExample.txt");
        StreamReader f1 = new StreamReader("inExample.txt");
        f.WriteLine(" Таблиця значень \n");
        metka:
        s = f1.ReadLine();
        if (s == null) goto metka1;
        x = Convert.ToDouble(s);
        y = Math.Sqrt(x * x / (2 + Math.Exp(4 *
                                            Math.Log(x))));
        f.WriteLine(" аргумент x = {0:F3} функція y = {1:e3} \n", x, y);
        goto metka;
        metka1:
        f.WriteLine(" Склав Петренко Іван{0} \n", s);
        f.Close();
        f1.Close();
    }
}